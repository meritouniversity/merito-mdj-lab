# MySQL DB installation and first steps

## Installation using Docker
With Docker you can choose one of the below options
1) You can run Docker container manually:
```shell script
docker run --rm -it --name db-mysql-container -e MYSQL_ROOT_PASSWORD=1234 -e MYSQL_USER=merito_user -e MYSQL_PASSWORD=qwerty -e MYSQL_DATABASE=mdj -p 3306:3306 mysql:latest
```
2) You can use docker-compose service script
```shell
cd ../docker
docker-compose -f compose.yml up -d db-mysql
```

## Installation test db schema
```shell script
cd ../sampleDB/actors
docker exec -i db-mysql-container mysql -umerito_user -pqwerty mdj < 01_schema.sql

```


Sample database `Sakila`:
https://dev.mysql.com/doc/sakila/en/sakila-installation.html


Installing test schema `world` with sample data
```shell
cd ../sampleDB/world
docker exec -i db-mysql-container mysql -uroot -p1234 < merito_user.sql
docker exec -i db-mysql-container mysql -umerito_user -pqwerty < world.sql
```

Installing test schema `sakila` with sample data
```shell
cd ../sampleDB/sakila
docker exec -i db-mysql-container mysql -uroot -p1234 < merito_user.sql
docker exec -i db-mysql-container mysql -umerito_user -pqwerty < sakila-schema.sql
docker exec -i db-mysql-container mysql -umerito_user -pqwerty < sakila-data.sql
```