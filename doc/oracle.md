# Postgres DB installation and first steps

## Installation using Docker
With Docker you can choose one of the below options
1) You can run Docker container manually:
```shell script
docker pull container-registry.oracle.com/database/free:latest
docker run --rm -it --name db-oracle-container -e ORACLE_PWD=qwerty -e ORACLE_SID=mdj -p 1521:1521 -v /var/oracle/oradata:/opt/oracle/oradata container-registry.oracle.com/database/free:latest
```
2) You can use docker-compose service script
```shell
cd ../docker
docker-compose -f compose.yml up -d db-psql
```

## Installation test db schema
```shell script
cd ../sampleDB/actors
docker exec -i db-psql-container psql -U merito_user mdj < 01_schema.sql

```