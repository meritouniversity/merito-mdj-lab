# Postgres DB installation and first steps

## Installation using Docker
With Docker you can choose one of the below options
1) You can run Docker container manually:
```shell script
docker run --rm -it --name db-psql-container -e POSTGRES_PASSWORD=qwerty -e POSTGRES_DB=mdj -e POSTGRES_USER=merito_user -p 5432:5432 postgres:latest
```
2) You can use docker-compose service script
```shell
cd ../docker
docker-compose -f compose.yml up -d db-psql
```

## Installation test db schema
```shell script
cd ../sampleDB/actors
docker exec -i db-psql-container psql -U merito_user mdj < 01_schema.sql
docker exec -i db-psql-container psql -U merito_user mdj < 02_actor.sql

```