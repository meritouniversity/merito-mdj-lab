DROP DATABASE IF EXISTS `sakila`;

CREATE DATABASE `sakila` DEFAULT CHARACTER SET utf8mb4;

GRANT ALL PRIVILEGES ON sakila.* TO merito_user;

SET GLOBAL log_bin_trust_function_creators = 1;

FLUSH PRIVILEGES;

