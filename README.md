# WSB Java DB

Database installation
* [MySQL DB](./doc/mysql.md)

* [MariaDB DB](./doc/mariadb.md)

* [PostgreSQL](./doc/psql.md)